I modified the processing of BlackBoard_new, UserHasNewBlackBoard messages, and added loginuserid

Student:
Receive BlackBoard_new message, get personal information, and send UserHasNewBlackBoard message

Reference Code：
SharePadMgr.java  
public void SendStudent() {
TKRoomManager.getInstance().pubMsg("UserHasNewBlackBoard", "_" + user.peerId,
                            "__all", new JSONObject(stuprepareing).toString(), true, "BlackBoard_new", user.peerId);

}

example：stuprepareing content

 "id" -> "1b4ecfdb-2f38-434b-b7f0-c2110a705f90"
 "nickname" -> "apple"
 "loginuserid" -> "id007"  //This is newly added
 "role" ->  2

 

Teacher:After receiving UserHasNewBlackBoard, you can get loginuserid

Reference Code：
SharePadMgr.java  
  case "UserHasNewBlackBoard": 
                acceptUserHasNewBlackBoard(jsonData);
        break;
	
jsonData Content：	
{"id":"1b4ecfdb-2f38-434b-b7f0-c2110a705f90","nickname":"apple","loginuserid":"id007","role":2}




When the teacher exits the room and re-enters the room, he will receive the UserHasNewBlackBoard message from the server
At this time, you can get loginuserid from jsonData.

Remarks：
The prerequisite is to add loginuserid to the json when sending UserHasNewBlackBoard message. Then the server will send these messages when the teacher is back online

The previous code SharePadMgr.java has these changes, you can refer to it.




