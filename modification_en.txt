This version modified the function of loading courseware
  The user selects the required course according to the selected grade and semester
  
  The json of the courseware directory is placed on the aws server courseware/courseware.json

  {"course":[
  {"level":"P2上","courseware":["P2上A_1.三位數", "P2上B_10.時和分"]},
  {"level":"P2下","courseware":["P2B13_除法(一)", "P2B20_四邊形(二)"]},
  {"level":"P5下","courseware":["P5B14_簡易方程（一）", "P5B4_小數乘法（一）"]},
  {"level":"P6下","courseware":["P6B3_折扣（一）"]}]}
  
  
   
 P2上 means last semester of second grade
 P2下 means the second semester of the second grade of elementary school
 courseware means courseware

 
 A complete courseware address in the program: for example, P5B14_簡易方程（一）
 courseware/P5下/P5B14_簡易方程（一）/

 

Mainly related code:
talkplus\eduhdsdk\src\main\java\com\eduhdsdk\viewutils\CoursewarePopupWindowUtils.java

public  HashMap<String, Object> ParseJsonStringToCourseware(String jsonStr);
reference:

talkplus\eduhdsdk\src\main\java\com\eduhdsdk\adapter\CoursewareListAdapter.java

Related layout files:
talkplus\eduhdsdk\src\main\res\layout\tk_layout_courseware_popupwindow.xml
talkplus\eduhdsdk\src\main\res\layout\tk_layout_courseware_list_item.xml
talkplus\eduhdsdk\src\main\res\layout\tk_layout_term_type_item.xml
talkplus\eduhdsdk\src\main\res\layout\tk_popup_courseware_title_layout.xml
talkplus\eduhdsdk\src\main\res\layout\tk_popup_courseware_list_layout.xml
