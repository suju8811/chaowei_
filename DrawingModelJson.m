//
//  DrawingModelJson.m
//  PhotonFECT
//
//  Created by Suju on 7/4/20.
//  Copyright © 2020 Suju. All rights reserved.
//

#import "DrawingModelJson.h"

@implementation DrawingModelJson

-(instancetype)initWithDictionary:(NSDictionary*) dict{
    self = [super init];
    
    //~----- sample-------
//    NSArray*points = [[[data objectForKey:@"data"] objectForKey:@"data"] objectForKey:@"pointCoordinatePairs"];
    
    
    
//~~~~~please convert follow android code to obj codes~~~~~
    
//    try {
//        JSONObject root=new JSONObject(jsonStr);
//
//        JSONArray arrays=root.getJSONArray("datas");
//        for (int i=0;i<arrays.length();i++){
//            JSONObject data=arrays.getJSONObject(i);
//
//            pageid = data.getInt("pageid");
//
//            JSONArray dataArrays=data.getJSONArray("pagedata");
//            for (int j=0;j<dataArrays.length();j++) {
//                //   JSONObject brushjson= data.getJSONObject("pagedata");
//                JSONObject brushjson=dataArrays.getJSONObject(j);
//                className = brushjson.getString("className");
//
//                switch (className) {
//                    case "LinePath":
//                    case "ErasedLinePath":
//                        JSONObject LineJson = brushjson.optJSONObject("data");
//                        penWidth = LineJson.opt("pointSize") instanceof String ? Integer.parseInt(LineJson.optString("pointSize")) : LineJson.optInt("pointSize");
//
//                        String colorpoint = LineJson.optString("pointColor");
//                        if (colorpoint.contains("#")) {
//                            penColor = Color.parseColor(colorpoint);
//                        }
//
//                        JSONArray pointCoordinatePairs = null;
//                        try {
//                            pointCoordinatePairs = new JSONArray(LineJson.optString("pointCoordinatePairs"));
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                        if (pointCoordinatePairs != null) {
//                            for (int k = 0; k < pointCoordinatePairs.length(); k++) {
//                                JSONArray ptfs = pointCoordinatePairs.optJSONArray(k);
//                                PointF p = new PointF();
//                                p.x = (float) ptfs.optDouble(0);
//                                p.y = (float) ptfs.optDouble(1);
//                                points.add(p);
//                            }
//
//
//                        }
//                        break;
//                    case "Text":
//                        JSONObject TextJson = brushjson.optJSONObject("data");
//                        String font = TextJson.optString("font");
//                        String[] str1 = font.split(" ");
//                        String font1 = null;
//                        if (str1.length == 4) {
//                            font1 = !str1[2].isEmpty() && str1[2] != null ? str1[2] : "18px";
//                            if (font1.endsWith("px")) {
//                                font1 = font1.substring(0, font1.indexOf("p"));
//                            }
//                        }
//
//                        penWidth = Integer.parseInt(font1 == null ? "18" : font1); //set default to 18 if not exit
//                        penColor = Color.parseColor(TextJson.optString("color"));
//
//                        text = TextJson.optString("text");
//                        PointF ttF = new PointF();
//                        ttF.x = (float) TextJson.optDouble("x");
//                        ttF.y = (float) TextJson.optDouble("y");
//
//
//                        break;
//                }
//
//            }
//        }
//
//    }  catch (JSONException e) {
//        e.printStackTrace();
//    }
    
    return self;
}


//please use these 2 functions on other obj-c files
- (void)doSomethingWithTheJson
{
    NSDictionary *dict = [self JSONFromFile_Test];
    self.gropModel = [[GroupModel alloc] initWithDictionary:dict];
}

- (NSDictionary *)JSONFromFile_Test:(NSString*)jsonTitle
{
    NSString *path = [[NSBundle mainBundle] pathForResource:jsonTitle ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    return [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
}

@end
