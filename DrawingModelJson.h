//
//  DrawingModelJson.h
//  PhotonFECT
//
//  Created by Suju on 7/4/20.
//  Copyright © 2020 Suju. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DrawingModelJson : NSObject
@property(nonatomic, assign)  int pageid;
@property (nonatomic, strong) NSString* className;

@property(nonatomic, assign)  int penWidth;
@property(nonatomic, assign)  int penColor;
@property (nonatomic, strong) NSString* text;
@property (nonatomic, strong) NSMutableArray <NSValue *>* points;

-(instancetype)initWithDictionary:(NSDictionary*) dict;
@end
