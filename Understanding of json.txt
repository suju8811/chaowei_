This is the data received by SharpChange.
{"codeID":"178368644","eventType":"shapeSaveEvent","actionName":"AddShapeAction","shapeId":"fdcb958b-e71e-400f-a32b-9402b4bf68d5","whiteboardID":"15c22d48-79ac-4bf1-bde9-5ed188bd345d","nickname":"3-id006","isBaseboard":false,"data":{"className":"LinePath","data":{"order":3,"tailSize":3,"smooth":true,"pointSize":14,"pointColor":"#000000","pointCoordinatePairs":[[724.24896,156.2399],[728.98596,173.26747],[733.7231,202.45764],[743.19714,248.67545],[747.93414,290.02823],[752.6713,336.24597],[757.4083,389.7613],[764.5139,445.70917],[769.25085,499.22452],[776.3565,552.73987],[783.462,601.39014],[788.19904,647.6079],[790.56757,684.09564],[795.30457,713.28577],[795.30457,737.61096],[795.30457,737.61096]]},"id":"fdcb958b-e71e-400f-a32b-9402b4bf68d5"}}


This is the data of the json file.
{"datas":[{"pageid":1,"pagedata":[{"className":"LinePath","data":{"pointSize":14,"pointColor":"#000000","pointCoordinatePairs":[[724.24896,156.2399],[728.98596,173.26747],[733.7231,202.45764],[743.19714,248.67545],[747.93414,290.02823],[752.6713,336.24597],[757.4083,389.7613],[764.5139,445.70917],[769.25085,499.22452],[776.3565,552.73987],[783.462,601.39014],[788.19904,647.6079],[790.56757,684.09564],[795.30457,713.28577],[795.30457,737.61096],[795.30457,737.61096]]}}]}]}



json file analysis：

pageid: Indicates the current page, Start from 1

The data of the current page is stored in pagedata, which is similar to the data in the received SharpsChange, except that some fixed values ​​are deleted
Because in the future we will still need to edit the class at the js layer, so some fixed information is deleted.
I suggest you refer to the code of ios or android. Because the message of SharpsChange is common in android and ios.


I also refer to the information of SharpChange to convert to json
